#!/usr/bin/python

import sys
import Queue
import logging

class PriceAnalysis(object):
    
    def __init__(self, data_object): # dict of lists of dicts and so on.
        self.data_object = data_object

    def time_variation(data_object): #  Single ISP
        ''' different types possible, per ISP, per country
        1. Range of price/MB in any given year
        2. Range of price/MB across the years
        3. Price/Mb by country '''
        
        


        
    def plan_variation(data_object):
        ''' two kinds possible
        1. Price/cap variation across different ISPs for a similar plan
        '''# 2. Price variation across 
        pass
    
    def market_classifier(data_object):
        ''' classify countries into different markets'''
        pass
    
    def  num_of_isps(data_object):
        ''' Variation in the number of ISPs'''
        pass
    
    def num_of_plans(data_object):
        ''' Variation in the no of plans(competing ISPs)
        variation in no of plans of an ISP with time'''
        pass


class PriceParser(object):

    def __init__(self, queue):
        self.File_queue = queue
        # print "init called\n", self.File_queue.qsize()

    def find_dict_in_list(self, query, temp_list):
        ''' Iterate throught the list of dicts and returns True or False
        double computation, but saves from the problem of putting stuff at
        the end of if blocks, rather in an additional else block'''

        for items in temp_list:
            if query in items:
                return True
            else:
                pass
            
        return False
        
    def parse(self):
        Unlimited = 1000000

        data_queue = self.File_queue
        print "size", data_queue.qsize()
        Dict_pname = {} # contains the country, isp,yr
        Dict_pdata = {} # contains index and plan details
    
        index = 0

        while not data_queue.empty():
            F = data_queue.get()
            F.readline()
            # print temp
            
            for line in F:
                # print line
                year = int(F.name[0:4])
                # logging.info("%s %s" %(year, line))
                index += 1
                # if line[0] == "#":  first line is only labels, no data
                #     continue # I don't want check for each line
                rec = line.rstrip().split(",")

                for i in range(len(rec)): # leading,trailing spaces
                    rec[i] = rec[i].strip()
                    
                if len(rec) < 6:
                    logging.info("%s %s" %(year, line))
                    print "length_issue"
                    continue;

                if len(rec) == 6: # 2002,2004 files
                    plan_details = []

                    #  some files have averaged prices by country
                    if not rec[1]:
                        logging.debug("[No D/L, U/L speeds]%s %s" %(year, line))
                        continue;

                    if (not rec[4]) and (not rec[5]):
                        logging.debug("[No D/L, U/L speeds]%s %s" %(year, line))
                        continue;
                    elif int(rec[4]) == 0 and int(rec[5]) == 0:
                        logging.debug("[No D/L, U/L speeds]%s %s" %(year, line))
                        continue;
                    else:
                        # print "done"
                        bitcap = {}
                        if rec[3] == "Unlimited" or rec[3] == "unlimited":
                            bitcap["bcap"] = Unlimited
                        else:
                            try:  # some entries like unlimited7 
                                if isinstance(int(rec[3]),int):
                                    if int(rec[3]) == 0:
                                        bitcap["bcap"] = Unlimited
                                    else:
                                        bitcap["bcap"] = int(rec[3])
                                else:
                                    bitcap["bcap"] = Unlimited
                            except ValueError:
                                bitcap["bcap"] = Unlimited

                        plan_details.append(bitcap)  # Bitcapacity

                        dl = {}
                        # if there is no d/l speed, the record is worthless
                        if not rec[4]:  
                            continue
                        else:
                            try:
                                if isinstance(int(rec[4]),int):
                                    dl["dl"] = int(rec[4])
                            except:
                                logging.info("%s %s" %(year,rec))
                                continue

                        plan_details.append(dl)  # d/l speed

                        ul = {}
                        if not rec[5]:
                             # ul speeds are not very important, can be ignored
                            ul["ul"] = 0 
                        else:
                            ul["ul"] = int(rec[5])
                        plan_details.append(ul) # u/l speed

                        Dict_pname[index] = plan_details
                        Dict_i_plans = {}
                        Dict_i_plans[index] = float(rec[2]) # cost dict


                        if rec[0] in Dict_pdata: #  country
                            if PriceParser.find_dict_in_list(self, year,
                                                               Dict_pdata[rec[0]]):
                                for temp_yr in Dict_pdata[rec[0]]:
                                    if year in temp_yr:
                                        if PriceParser.find_dict_in_list(
                                            self, rec[1],temp_yr[year]):
                                            for temp_isp in temp_yr[year]:
                                                if rec[1] in temp_isp:  #  isp
                                                    temp_isp[rec[1]].append(
                                                        Dict_i_plans)
                                                    break
                                                else:
                                                    pass
                                        else:
                                            temp_isp = {}
                                            temp_isp[rec[1]] = [] 
                                            temp_isp[rec[1]].append(Dict_i_plans)
                                            temp_yr[year].append(temp_isp)
                                            break                         
                                    else:
                                        pass    

                            else:
                                temp_isp = {}
                                temp_isp[rec[1]] = []
                                temp_isp[rec[1]].append(Dict_i_plans)
                                temp_yr = {}
                                temp_yr[year] = []
                                temp_yr[year].append(temp_isp)
                                Dict_pdata[rec[0]].append(temp_yr)
                                # break
                                continue
                                
                        else:
                            Dict_tmpisp = {}
                            Dict_tmpisp[rec[1]] = []
                            Dict_tmpisp[rec[1]].append(Dict_i_plans)
                            Dict_tmpyr = {}
                            Dict_tmpyr[year] = []
                            Dict_tmpyr[year].append(Dict_tmpisp)
                            Dict_pdata[rec[0]]  = []
                            Dict_pdata[rec[0]].append(Dict_tmpyr)
                            continue
                            # break
                                
                elif len(rec) == 8:  # 2006,2008,2010 file
                    plan_details = []
                    #  necessary as, there are entries like 1 500 and 80 000
                    for i in range(4, len(rec)): 
                        if rec[i].find(" ") is not -1:
                            temp_rec = rec[i].split(" ")
                            rec[i] = temp_rec[0] + temp_rec[1]

                    if not rec[1]:
                        logging.debug("[No D/L, U/L speeds]%s %s" %(year, line))
                        continue;


                    if (not rec[4]) and (not rec[5]):
                        logging.debug("%s %s" %(year, line))
                        continue;
                    else:
                        bitcap = {}
                        if rec[6] == "Unlimited" or rec[6] == "unlimited":
                            bitcap["bcap"] = Unlimited
                        else:
                            try:  # some entries like unlimited7 
                                if isinstance(int(rec[6]),int):
                                    if int(rec[6]) == 0:
                                        bitcap["bcap"] = Unlimited
                                    else:
                                        bitcap["bcap"] = int(rec[6])
                                else:
                                    bitcap["bcap"] = Unlimited
                            except ValueError:
                                bitcap["bcap"] = Unlimited

                            plan_details.append(bitcap)  # Bitcapacity

                            dl = {}
                            # if there is no d/l speed, the record is worthless
                            if not rec[4]: 
                                logging.DEBUG("No D/L speed %s %s" %(year,line))
                                continue

                            else:
                                try:
                                    if isinstance(int(rec[4]),int):
                                        dl["dl"] = int(rec[4])
                                except:
                                    logging.debug("%s %s" %(year,rec[1]))
                                    continue

                            plan_details.append(dl)  # d/l speed

                            ul = {}
                            if not rec[5]:
                                 # ul speeds are not very important, can be ignored
                                ul["ul"] = 0 
                            else:
                                ul["ul"] = int(rec[5])
                            plan_details.append(ul) # u/l speed

                            tech_name = {}
                            if not rec[2]:
                                tech_name["tech_name"] = "NA"
                            else:
                                tech_name["tech_name"] = rec[2]
                            plan_details.append(tech_name)

                            plan_name = {}
                            if not rec[3]:
                                plan_name["plan_name"] = "NA"
                            else:
                                plan_name["plan_name"] = rec[3]
                            plan_details.append(plan_name)

                            Dict_pname[index] = plan_details
                            Dict_i_plans = {}
                            Dict_i_plans[index] = float(rec[7]) # cost dict

                            if rec[0] in Dict_pdata:  # country
                                if PriceParser.find_dict_in_list(
                                    self, year, Dict_pdata[rec[0]]):
                                    # print "DEBUG_Y"
                                    for temp_yr in Dict_pdata[rec[0]]:
                                        if year in temp_yr:  # year
                                            if PriceParser.find_dict_in_list(
                                                self,rec[1],temp_yr[year]):
                                                # print "DEBUG_Y"
                                                for temp_isp in temp_yr[year]:
                                                    if rec[1] in temp_isp:  # isp
                                                        temp_isp[rec[1]].append(
                                                            Dict_i_plans)
                                                        break
                                                    else:
                                                        pass

                                            else:
                                                # print "DEBUG_N"
                                                temp_isp = {}
                                                temp_isp[rec[1]] = []
                                                temp_isp[rec[1]].append(Dict_i_plans)
                                                temp_yr[year].append(temp_isp)
                                                break
                                        else:
                                            pass

                                else:
                                    # print "DEBUG_N"
                                    temp_isp = {}
                                    temp_isp[rec[1]] = []
                                    temp_isp[rec[1]].append(Dict_i_plans)
                                    temp_yr = {}
                                    temp_yr[year] = []
                                    temp_yr[year].append(temp_isp)
                                    Dict_pdata[rec[0]].append(temp_yr)
                                    continue
                            else:
                                Dict_tmpisp = {}
                                Dict_tmpisp[rec[1]] = []
                                Dict_tmpisp[rec[1]].append(Dict_i_plans)
                                Dict_tmpyr = {}
                                Dict_tmpyr[year] = []                                
                                Dict_tmpyr[year].append(Dict_tmpisp)
                                # Dict_pdata = {}
                                Dict_pdata[rec[0]] = []
                                Dict_pdata[rec[0]].append(Dict_tmpyr)
                                continue

                elif len(rec) == 9:  # 2011 file
                    #  includes an additional bundle flag
                    plan_details = []

                    for i in range(4, len(rec)): 
                        if rec[i].find(" ") is not -1:
                            temp_rec = rec[i].split(" ")
                            rec[i] = temp_rec[0] + temp_rec[1]

                    if not rec[1]:
                        logging.debug("[No ISP Name]%s %s" %(year, line))
                        continue;


                    if not rec[4] and not rec[5] and not rec[8]:
                        logging.debug("[No D/L, U/L speed and cost]%s %s" %(year, line))
                        continue;
                    else:
                        bitcap = {}
                        if rec[7] == "Unlimited" or rec[7] == "unlimited":
                            bitcap["bcap"] = Unlimited
                        else:
                            try:  # some entries like unlimited7 
                                if isinstance(int(rec[7]),int):
                                    if int(rec[7]) == 0:
                                        bitcap["bcap"] = Unlimited
                                    else:
                                        bitcap["bcap"] = int(rec[7])
                                else:
                                    bitcap["bcap"] = Unlimited
                            except ValueError:
                                bitcap["bcap"] = Unlimited

                        plan_details.append(bitcap)  # Bitcapacity

                        dl = {}
                        # if there is no d/l speed, the record is worthless
                        if not rec[4]: 
                            continue
                        else:
                            try:
                                if isinstance(float(rec[4]),float):
                                    dl["dl"] = float(rec[4])
                            except:
                                logging.info("[D/L speed]%s %s" %(year,rec))
                                continue
                        plan_details.append(dl)  # d/l speed

                        ul = {}
                        if not rec[5]:
                             # ul speeds are not very important, can be ignored
                            ul["ul"] = 0 
                        else:
                            try:
                                if isinstance(float(rec[5]),float):
                                    ul["ul"] = float(rec[5])
                            except:
                                ul["ul"] = 0
                                
                        plan_details.append(ul) # u/l speed

                        tech_name = {}
                        if not rec[2]:
                            tech_name["tech_name"] = "NA"
                        else:
                            tech_name["tech_name"] = rec[2]
                        plan_details.append(tech_name)

                        plan_name = {}
                        if not rec[3]:
                            plan_name["plan_name"] = "NA"
                        else:
                            plan_name["plan_name"] = rec[3]
                        plan_details.append(plan_name)

                        is_bundle = False
                        if int(rec[6]) == 1:
                            is_bundle = True
                        plan_details.append(is_bundle)

                        Dict_pname[index] = plan_details
                        Dict_i_plans = {}
                        if rec[8] == "NA":
                            logging.debug("Cost N/A %s %s" %(year, rec))
                            continue
                        else:
                            try:
                                if isinstance(float(rec[8]),float):
                                    Dict_i_plans[index] = float(rec[8]) # cost dict
                            except:
                                logging.error("No cost Information %s %s" %(year, line))
                                continue

                        if rec[0] in Dict_pdata:  # country
                            if PriceParser.find_dict_in_list(
                                self, year, Dict_pdata[rec[0]]):
                                for temp_yr in Dict_pdata[rec[0]]:
                                    if year in temp_yr:  # year
                                        if PriceParser.find_dict_in_list(
                                            self, rec[1],temp_yr[year]):
                                            for temp_isp in temp_yr[year]:
                                                if rec[1] in temp_isp:  # isp
                                                    temp_isp[rec[1]].append(
                                                        Dict_i_plans)
                                                    break
                                                else:
                                                    pass
                                        else:
                                            temp_isp = {}
                                            temp_isp[rec[1]] = []
                                            temp_isp[rec[1]].append(
                                                Dict_i_plans)
                                            temp_yr[year].append(temp_isp)
                                            break
                                    else:
                                        pass
                                    # break
                            else:
                                temp_isp = {}
                                temp_isp[rec[1]] = []
                                temp_isp[rec[1]].append(Dict_i_plans)
                                temp_yr = {}
                                temp_yr[year] = []
                                temp_yr[year].append(temp_isp)
                                Dict_pdata[rec[0]].append(temp_yr)
                                continue

                        else:
                            Dict_tmpisp = {}
                            Dict_tmpisp[rec[1]] = []
                            Dict_tmpisp[rec[1]].append(Dict_i_plans)
                            Dict_tmpyr = {}
                            Dict_tmpyr[year] = []
                            Dict_tmpyr[year].append(Dict_tmpisp)
                            Dict_pdata[rec[0]] = []
                            Dict_pdata[rec[0]].append(Dict_tmpyr)
                            continue

        for k,v in Dict_pname.iteritems():
            print k,v
        for k,v in Dict_pdata.iteritems():
            print k,v

        return Dict_pdata

def main():

    logging.basicConfig(filename = 'pricing_analysis.log', level = logging.DEBUG)
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    
    # try:
    File_queue = Queue.Queue()
    for i in range(1, len(sys.argv)):
        File_queue.put(open(sys.argv[i],'r'))
        
    price_map_obj = PriceParser(File_queue)
    price_map = price_map_obj.parse()

    # for k,v in price_map.iteritems():
    #     print k,v

if __name__ == "__main__":
    main()
